//
//  ContentView.swift
//  Project2GuessTheFlag
//
//  Created by AJAY on 06/07/20.
//  Copyright © 2020 Individual. All rights reserved.
//

import SwiftUI

struct ContentViewBasic: View {
    
    //Topic 5 reference
    @State private var showingAlert = false
    
    var body: some View {
        
        //Topic 1 : Using Stacks To arrange views
        
        
        /* When we return some View for our body, we mean “one specific type that conforms to the View protocol. That might be a navigation view, a form, a text view, a picker, or something else entirely, but it’s always exactly one thing that conforms to the View protocol.
         
         If we want to return multiple things we have various options, but three are particularly useful. They are HStack, VStack, and ZStack, which handle horizontal, vertical, and, er, zepth.
         
         e.g If we wanted to return two text views, this kind of code simply isn’t allowed:
         
         Text("Hello, World!")
         Text("This is another text view")
         
         will throw error as we are returning multiple views , allowed only one.Here come stack opetion
         
         */
        
        
        /*Note: stack
         
         1.Just like SwiftUI’s other views, VStack can have a maximum of 10 children – if you want to add more, you should wrap them inside a Group.

         2.By default, VStack aligns its views so they are centered, but you can control that with its alignment property. For example, this aligns the text views to their leading edge, which in a left-to-right language such as English will cause them to be aligned to the left:
         
         3. Setting space vertically between two Text view
         
         4. Vertical and horizontal stacks automatically fit their content, and prefer to align themselves to the center of the available space. If you want to change that you can use one or more Spacer views to push the contents of your stack to one side. These automatically take up all remaining space, so if you add one at the end a VStack it will push all your views to the top of the screen:
         
         5.Zstack :
              A.We also have ZStack for arranging things by depth – it makes views that overlap. In the case of our two text views, this will make things rather hard to read
            B.ZStack doesn’t have the concept of spacing because the views overlap, but it does have alignment. So, if you have one large thing and one small thing inside your ZStack, you can make both views align to the top like this: ZStack(alignment: .top) {.

            ZStack draws its contents from top to bottom, back to front. This means if you have an image then some text ZStack will draw them in that order, placing the text on top of the image.
         */
    
        
        
//        VStack(alignment: .leading, spacing: 20) //Setting space vertically between two Text view
//        {
//            Text("Hello World")
//            Text("This is inside a stack")
//             Spacer()
//        }
        
//        HStack(spacing: 20) {
//            Text("Hello World")
//            Text("This is inside a stack")
//            Spacer()
//        }
//
//        ZStack (alignment: .top){
//            Text("Hello World")
//            Text("This is inside a stack")
//        }
        
        
        
        
        //Topic 2: Colors and frames
        
        
        
   //     ZStack {
            
           // Color.red
            //whole stack area background color
            
            
           // Color.red.frame(width: 200, height: 200)
            //Using Frame i.e stack will be also same frame
            
            
    //        Color.red.edgesIgnoringSafeArea(.all)
            
            //Here stack vieww will be within safe area but color will be full area
            
            
          /*  If you want your content to go under the safe area, you can use the edgesIgnoringSafeArea() modifier to specify which screen edges you want to run up to. For example, this creates a ZStack which fills the screen edge to edge with red then draws some text on top:*/
            
         //   Text("Your content")
   //     }.background(Color.red).border(Color.black) //Make only text are background as red
        
        
        
        
        //Topic 3: Gradients
        
        
        /*Gradients are made up of several components:

       1. An array of colors to show
       2. Size and direction information
       3. The type of gradient to use
         
         
         1. LinearGradient(gradient: Gradient(colors: [.white, .black]), startPoint: .top, endPoint: .bottom)
         
              a linear gradient goes in one direction, so we provide it with a start and end point like this:
         
         2.RadialGradient(gradient: Gradient(colors: [.blue, .black]), center: .center, startRadius: 20, endRadius: 200)
         
         radial gradients move outward in a circle shape, so instead of specifying a direction we specify a start and end radius – how far from the center of the circle the color should start and stop changing.
         
         3.AngularGradient(gradient: Gradient(colors: [.red, .yellow, .green, .blue, .purple, .red]), center: .center)
         
         
         The last gradient type is called an angular gradient, although you might have heard it referred to elsewhere as a conic or conical gradient. This cycles colors around a circle rather than radiating outward, and can create some beautiful effects.
         */
        
//        ZStack {
//            LinearGradient(gradient: Gradient(colors: [.blue, .white, .pink]), startPoint: .top, endPoint: .bottom)
//            Text("Testing Gradient Color ")
//        }.edgesIgnoringSafeArea(.all)
        
        
        
        
        //Topic 4 => Buttons with images set opn it
        
        
      //  Type 1 : The simplest way to make a button is when it just contains some text: you pass in the title of the button, along with a closure that should be run when the button is tapped:
        
//        Button("Tap Me"){
//            print("Button was tapped")
//        }
        
        //Type 2 : If you want something more, such as an image or a combination of views, you can use this alternative form
        
        
        
        /*
         
         Types of images => Important
         
         1. Image("pencil") will load an image called “Pencil” that you have added to your project.
        2. Image(decorative: "pencil") will load the same image, but won’t read it out for users who have enabled the screen reader. This is useful for images that don’t convey additional important information.
        3. Image(systemName: "pencil") will load the pencil icon that is built into iOS. This uses Apple’s SF Symbols icon collection, and you can search for icons you like – download Apple’s free SF Symbols app from the web to see the full set.
         
         
         Tip: If you find that your images have become filled in with a color, for example showing as solid blue rather than your actual picture, this is probably SwiftUI coloring them to show that they are tappable. To fix the problem, use the renderingMode(.original) modifier to force SwiftUI to show the original image rather than the recolored version.
         
         */
        
        
        
        
        
//        Button(action: {
//            print("Edit button was tapped")
//        }) {
//            HStack(spacing: 10) {
//
//                Image(systemName: "pencil")
//                    .renderingMode(.original) //Without it will show blue color pencil system image
//                Text("Edit")
//            }
//        }
        
        
        //Topic 5 : Showing alert messages
        
        
        /* We then attach our alert somewhere to our user interface, telling it to use that state to determine whether the alert is presented or not. SwiftUI will watch showingAlert, and as soon as it becomes true it will show the alert.
         
        **** That’s another two-way data binding, and it’s here because SwiftUI will automatically set showingAlert back to false when the alert is dismissed.
         
         In below example That attaches the alert to the button, but honestly it doesn’t matter where the alert() modifier is used – all we’re doing is saying that an alert exists and is shown when showingAlert is true.
         
         */
        
        Button("Show Alert") {
            self.showingAlert = true
        }
        .alert(isPresented: $showingAlert) {
            Alert(title: Text("Hello SwiftUI!"), message: Text("This is some detail message"), dismissButton: .default(Text("OK")))
        }
        
        
        
    }
}

struct ContentView_PreviewsBasic: PreviewProvider {
    static var previews: some View {
        ContentViewBasic()
    }
}
